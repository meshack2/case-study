import { useState, useEffect } from "react";
import { allHotels } from "../actions/hotel";
import HotelCard from "../components/cards/HotelCard";

const Home = () => {
  const [hotels, setHotels] = useState([]);

  useEffect(() => {
    loadAllhotels();
  }, []);

  const loadAllhotels = async () => {
    let res = await allHotels();
    console.log(res.data)
    setHotels(res.data);
  };

  return (
    <>
      <div className="container bg-blue-100 p-10 text-center">
        <h1 className="text-yellow-400">All Hotels</h1>
      </div>
      <div className="col-auto">
        <br />
      </div>
      <div className="container">
        <br />
        {hotels.map((h) => (
          <HotelCard key={h.id} h={h} />
        ))}
      </div>
    </>
  );
};

export default Home;
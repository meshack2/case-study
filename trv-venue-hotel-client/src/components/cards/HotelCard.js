import { useHistory } from "react-router-dom";
import StarRatings from 'react-star-ratings'

const HotelCard = ({
  h,
}) => {
  const history = useHistory();
  return (
    <>
      <div className="card d-flex justify-content-center w-100 mb-3">
        <div className="row no-gutters">
          <div className="col-md-3">
            <img
            src={h.image}
            alt="default hotel"
            className="card-image img img-fluid"
            />
          </div>
          <div className="col-md-5">
            <div className="card-body">
              <h3 className="card-title">
                {h.name}
              </h3>
              <StarRatings
                rating={h.rating}
                starDimension="20px"
                starRatedColor="gold"
                numberOfStars={5}
                name='rating'
               />
              <p className="float-right"><small>{h.distance_to_venue} miles to Venue</small></p>
              <p className="card-text">Price Category: <strong>{h.price_category}</strong></p>
              <p className="card-text">
                {/* {h.amenities.split(', ').map((amenity) => (
                    <span key={amenity} className="float-right text-primary">{amenity}</span>
                ))} */}
              </p>
              <button
                    onClick={() => history.push(`/hotel/${h.id}`)}
                    className="btn btn-primary"
              >
                    Show Hotel Rooms
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HotelCard;

import { BrowserRouter, Switch, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TopNav from "./components/TopNav";
// import PrivateRoute from "./components/PrivateRoute";
// components
import Home from "./booking/Home";
// import Login from "./auth/Login";
// import Dashboard from "./user/Dashboard";
// import StripeCallback from "./stripe/StripeCallback";
import HotelRooms from "./hotels/HotelRoomsDetail";
// import StripeSuccess from "./stripe/StripeSuccess";
// import StripeCancel from "./stripe/StripeCancel";
// import SearchResult from "./hotels/SearchResult";

function App() {
  return (
    <BrowserRouter>
      <TopNav />
      <ToastContainer position="top-center" />
      <Switch>
        <Route exact path="/" component={Home} />
        {/* <Route exact path="/login" component={Login} />
        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <PrivateRoute
          exact
          path="/stripe/callback"
          component={StripeCallback}
        /> */}
        <Route exact path="/hotel/:hotelId" component={HotelRooms} />
        {/* <PrivateRoute
          exact
          path="/stripe/success/:hotelId"
          component={StripeSuccess}
        />
        <PrivateRoute exact path="/stripe/cancel" component={StripeCancel} />
        <Route exact path="/search-result" component={SearchResult} /> */}
      </Switch>
    </BrowserRouter>
  );
}

export default App;

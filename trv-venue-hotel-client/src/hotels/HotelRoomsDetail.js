import React, { useState, useEffect } from "react";
import { read} from "../actions/hotel";

const HotelRooms = ({ match, history }) => {
  const [hotel, setHotel] = useState({});

  useEffect(() => {
    loadHotelRooms();
  }, []);

  const loadHotelRooms = async () => {
    let res = await read(match.params.hotelId);
    setHotel(res.data);
    console.log(Object.keys(res.data.rooms))
  };

  return (
    <>
    <div className="card d-flex justify-content-center w-100 mb-3">
        <div className="col no-gutters">
          {hotel.rooms &&
                 Object.keys(hotel.rooms).map((room, i) => (
                <div className="col-md-5">
                  <div className="card-body">
                    <h3 className="card-title">
                      {hotel.rooms[room].name}
                    </h3>
                    <p className="float-right"><small>{hotel.rooms[room].description}</small></p>
                    <p className="card-text">Available: <strong>{hotel.rooms[room].available}</strong></p>
                    <button
                        disabled={hotel.rooms[room].available === "false"}
                        onClick={() => history.push(`/hotel/${hotel.id}/book`)}
                        className="btn btn-primary"
                    >
                          Book Room
                    </button>
                  </div>
                </div>
                 )) 
          }
        </div>
      </div>
    </>
  );
};

export default HotelRooms;
